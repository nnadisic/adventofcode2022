
mutable struct Monkey
    items::Vector{Int}
    operation::String
    testdiv::Int
    targetiftrue::Int # Index of monkey to throw item to if test is true
    targetiffalse::Int # Index of monkey to throw item to if test is true
    counter::Int
end


function parseop(op::String, old::Int)
    old = old
    exp = Meta.parse(replace(op, "old"=>"$(old)"))
    return eval(exp)
end


function run(filename::String; nbrounds::Int=20, verbose::Bool=false)
    # Parse data
    data = read(filename, String)
    monkeys = Vector{Monkey}()
    for m in split(data, "\n\n")
        sm = split(m, "\n")
        items = [parse(Int, x) for x in split(split(sm[2], ": ")[2], ", ")]
        operation = split(sm[3], "= ")[2]
        testdiv = parse(Int, split(sm[4])[end])
        targetiftrue = parse(Int, split(sm[5])[end])
        targetiffalse = parse(Int, split(sm[6])[end])
        push!(monkeys, Monkey(items, operation, testdiv, targetiftrue, targetiffalse, 0))
    end

    # Play rounds
    for r in 1:nbrounds
        verbose && println("Round $(r)\n")
        for (idmk, curmk) in enumerate(monkeys)
            verbose && println("Monkey $(idmk)")
            for (id, item) in enumerate(curmk.items)
                curmk.counter += 1
                worry = round(Int, parseop(curmk.operation, item) / 3, RoundDown)
                target = iszero(worry % curmk.testdiv) ? curmk.targetiftrue : curmk.targetiffalse
                target += 1 # in AoC indices start at 0
                verbose && println("worry $(worry)\n target $(target)")
                push!(monkeys[target].items, worry)
            end
            curmk.items = []
            verbose && println()
        end
    verbose && println("########################################")
    end
    verbose && display(monkeys)

    # Compute the monkey business value
    counters = [mk.counter for mk in monkeys]
    bv = reduce(*, sort(counters)[end-1:end])

    return bv
end


# Run both parts
display(run("./example", nbrounds=20, verbose=false))
display(run("./input"))
