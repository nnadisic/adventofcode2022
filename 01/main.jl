using DelimitedFiles

function run(filename::String)
    data = read(filename, String)
    elves = split(data, "\n\n")
    caloriesbyelve = zeros(Int, length(elves))
    for (index, elve) in enumerate(elves)
        calories = [parse(Int, c) for c in split(elve)]
        caloriesbyelve[index] = sum(calories)
    end
    return maximum(caloriesbyelve)
end

display(run("./example"))
display(run("./input"))

function run2(filename::String)
    data = read(filename, String)
    elves = split(data, "\n\n")
    caloriesbyelve = zeros(Int, length(elves))
    for (index, elve) in enumerate(elves)
        calories = [parse(Int, c) for c in split(elve)]
        caloriesbyelve[index] = sum(calories)
    end
    return sum(sort(caloriesbyelve)[end-2:end])
end

display(run2("./example"))
display(run2("./input"))
