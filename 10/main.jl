using DelimitedFiles

function run(filename::String)
    # Read data as 2-column matrix
    data = readdlm(filename)
    # Init vector to store all register values
    registerstates = Vector{Int}(undef, size(data, 1)*2)
    registerstates[1] = 1
    # Loop on commands
    cycle = 1
    for (command, value) in eachrow(data)
        cycle += 1
        registerstates[cycle] = registerstates[cycle-1]
        if command != "noop"
            cycle += 1
            registerstates[cycle] = registerstates[cycle-1] + value
        end
    end

    # Draw output for Part 2
    for (cycle, state) in enumerate(registerstates)
        if  abs(cycle % 40 - state - 1) <= 1
            print("#")
        else
            print(".")
        end
        if iszero(cycle % 40)
            println()
        end
    end
    println()

    # Return output of Part 1
    signalsofinterest = collect(20:40:220)
    return sum(signalsofinterest .* registerstates[signalsofinterest])
end


# Run both parts
display(run("./example")) # 13140
display(run("./input"))
