using DelimitedFiles

function areadjacent(c1::CartesianIndex, c2::CartesianIndex)
    return maximum(abs.(Tuple(c1-c2))) <= 1 ? true : false
end


function run(filename::String)
    # Read data as 2-column matrix
    data = readdlm(filename)
    nbcommands = sum(data[:,2]) + 1
    # Init correspondence letter->move
    moves = Dict("U"=>CartesianIndex(1,0),
                 "R"=>CartesianIndex(0,1),
                 "D"=>CartesianIndex(-1,0),
                 "L"=>CartesianIndex(0,-1))
    # Store all positions history in a vector
    head_positions = Vector{CartesianIndex}(undef, nbcommands)
    tail_positions = Vector{CartesianIndex}(undef, nbcommands)
    # Init with starting point
    head_positions[1] = CartesianIndex(1,1)
    tail_positions[1] = CartesianIndex(1,1)

    # Init time/iteration index
    t = 1
    # Do moves
    for (direction, distance) in eachrow(data)
        for _ in 1:distance
            t += 1
            head_positions[t] = head_positions[t-1] + moves[direction]
            if areadjacent(head_positions[t], tail_positions[t-1])
                tail_positions[t] = tail_positions[t-1]
            else
                tail_positions[t] = head_positions[t-1]
            end
        end
    end

    return length(unique(tail_positions))
end



function run2(filename::String, nbknots=10)
    # Read data as 2-column matrix
    data = readdlm(filename)
    nbcommands = sum(data[:,2]) + 1
    # Init correspondence letter->move
    moves = Dict("U"=>CartesianIndex(1,0),
                 "R"=>CartesianIndex(0,1),
                 "D"=>CartesianIndex(-1,0),
                 "L"=>CartesianIndex(0,-1))
    # Store all positions history in a matrix
    # Each row is a moment in time
    # Each col is a knot, first one is head, then 8 knots then tail
    knots_positions = Matrix{CartesianIndex}(undef, nbcommands, nbknots)
    # At time 1 init all knots to start positon (1,1)
    knots_positions[1,:] .= [CartesianIndex(1,1) for _ in 1:nbknots]
    # Init time/iteration index
    t = 1
    # Do moves
    for (direction, distance) in eachrow(data)
        for _ in 1:distance
            t += 1
            knots_positions[t,1] = knots_positions[t-1,1] + moves[direction]
            for k in 2:nbknots
                if areadjacent(knots_positions[t,k-1], knots_positions[t-1,k])
                    knots_positions[t,k] = knots_positions[t-1,k]
                else
                    drow, dcol = Tuple(knots_positions[t,k-1] - knots_positions[t-1,k])
                    move = CartesianIndex(drow > 0 ? 1 : drow < 0 ? -1 : 0,
                                          dcol > 0 ? 1 : dcol < 0 ? -1 : 0)
                    knots_positions[t,k] = knots_positions[t-1,k] + move
                end
            end
        end
    end

    return length(unique(knots_positions[:,end]))
end


# Run part 1
# display(run("./example")) # 13
# display(run("./input")) # 6081

# Run part 1 with code from part 2
display(run2("./example", 2)) # 13
display(run2("./input", 2)) # 6081

# Run part 2
display(run2("./example", 10)) # 1
display(run2("./example2", 10)) # 36
display(run2("./input", 10))

# Debug part 2
# Must change return of run2 to """return knots_positions"""
# using Plots
# pos = run2("./example2", 10)
# indx = pos[:,end]
# scatter(getindex.(indx, 2), getindex.(indx, 1))
