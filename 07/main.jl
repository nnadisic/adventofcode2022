using DelimitedFiles

# mutable struct TreeNode
#     parent::String
#     children::Vector{String}
# end

# struct Tree
#     nodes::Vector{TreeNode}
# end

function run(filename::String, part::Int=1)
    # Read data
    lines = readlines(filename)
    # Store directories and contents
    filesystem = Dict{String, Vector{String}}()
    # Store sizes of files (and not dirs)
    filesizes = Dict{String, BigInt}()
    # The folder I am currently in
    curfolder = ""
    # Remember the full path to the current folder
    curpath = Vector{String}()
    # Parse the data line by line
    for line in lines
        tokens = split(line)
        # If line is a command, it is either change directory or list content
        if tokens[1] == "\$"
            command = tokens[2]
            if command == "cd"
                targetfolder = tokens[3]
                if targetfolder == ".."
                    pop!(curpath)
                    curfolder = curpath[end]
                else
                    curfolder = curfolder * "/" * targetfolder
                    filesystem[curfolder] = Vector{String}()
                    push!(curpath, curfolder)
                end
            elseif command == "ls"
                # do nothing
            else
                error("Unknown command")
            end
        else
            # if line is not a command, then it is the content of current folder
            if tokens[1] == "dir"
                push!(filesystem[curfolder], curfolder * "/" * String(tokens[2]))
            else # if not a dir then it is a file
                size, file = tokens
                push!(filesystem[curfolder], curfolder * "/" * file)
                filesizes[(curfolder * "/" * file)] = parse(BigInt, size)
            end
        end
    end
    # display(filesystem)
    # display(filesizes)

    # First compute the sizes of all directories
    dirsizes = Dict{String, BigInt}()
    tocompute = collect(keys(filesystem))
    while !isempty(tocompute)
        for dir in tocompute
            dirsize = 0
            for entry in filesystem[dir]
                # Look for the size this entry in files and in dirs
                entrysize = max(get(filesizes, entry, -1), get(dirsizes, entry, -1))
                # If present add it, if not present, abort current dir and break
                if entrysize != -1
                    dirsize += entrysize
                else
                    dirsize = 0
                    break
                end
            end
            if dirsize != 0
                dirsizes[dir] = dirsize
                deleteat!(tocompute, findall(x->x==dir, tocompute))
            end
        end
    end
    # display(dirsizes)

    # Init output
    output = zero(BigInt)

    # Part 1 find the dir of size at most 100000 and return sum of their sizes
    if part == 1
        for size in values(dirsizes)
            if size <= 100000
                output += size
            end
        end
    else
        totalspace = 70000000
        neededspace = 30000000
        spacetofree = neededspace - (totalspace - dirsizes["//"])
        validdirs = Vector{BigInt}() # sizes of directories whose deletion would free enough space
        for size in values(dirsizes)
            if size > spacetofree
                push!(validdirs, size)
            end
        end
        output = minimum(validdirs)
    end


    return output
end

# Run part 1
display(run("./example")) # 95437
display(run("./input"))

display(run("./example", 2)) # 24933642
display(run("./input", 2))
