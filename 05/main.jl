using DelimitedFiles


function run(filename::String, part::Int=1)
    # Read data as one big string
    data = read(filename, String)
    # Split the board part and moves part
    board, rawmoves = split(data, "\n\n")

    # Parse board
    boardlines = split(board, "\n")
    nbstacks = length(split(boardlines[end]))
    stacks = [Vector{String}() for _ in 1:nbstacks]
    # Read all lines except the last one with numbers
    for line in boardlines[1:end-1]
        l = replace(line, "    "=>" 0 ", "["=>" ", "]"=>" ")
        for (idx, crate) in enumerate(split(l))
            if crate != "0"
                pushfirst!(stacks[idx], crate)
            end
        end
    end

    # Parse moves
    movelines = split(rawmoves, "\n")[1:end-1] # last line is empty
    # Store moves in 3 columns: number, origin, destination
    moves = zeros(Int, length(movelines), 3)
    for (idx, line) in enumerate(movelines)
        sl = split(line)
        moves[idx, :] .= [parse(Int, val) for val in sl[[2,4,6]]]
    end

    # Do the moves
    if part == 1
        for move in eachrow(moves)
            number, origin, dest = move
            for _ in 1:number
                crate = pop!(stacks[origin])
                push!(stacks[dest], crate)
            end
        end
    else # part 2
        for move in eachrow(moves)
            number, origin, dest = move
            curcrates = Vector{String}()
            for _ in 1:number
                crate = pop!(stacks[origin])
                pushfirst!(curcrates, crate)
            end
            push!(stacks[dest], curcrates...)
        end
    end

    # Prepare output
    output = reduce(*, [last(stack) for stack in stacks])

    return output
end

# Run part 1
display(run("./example"))
display(run("./input"))

# Run part 2
display(run("./example", 2))
display(run("./input", 2))
