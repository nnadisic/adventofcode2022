
function run(filename::String)
    # Read data as one string
    data = read(filename, String)
    # Parse the grid in input as a matrix of Int
    grid = vcat([[parse(Int, entry) for entry in split(row, "")]' for row in split(data)]...)
    nbrow, nbcol = size(grid)

    nbvisible = 0
    for ci in CartesianIndices(grid)
        curtree = grid[ci]
        (i, j) = Tuple(ci)
        # If on the edge, it is always visible
        if i == 1 || i == nbrow || j == 1 || j == nbcol
            nbvisible += 1
        # If not, test the 4 directions
        else
            if minimum([maximum(dir) for dir in [grid[1:i-1,j],
                                                 grid[i+1:end,j],
                                                 grid[i,1:j-1],
                                                 grid[i,j+1:end]]]) < curtree
                nbvisible += 1
            end
        end
    end

    return nbvisible
end


function run2(filename::String)
    # Read data as one string
    data = read(filename, String)
    # Parse the grid in input as a matrix of Int
    grid = vcat([[parse(Int, entry) for entry in split(row, "")]' for row in split(data)]...)
    nbrow, nbcol = size(grid)

    scenicscores = zeros(Int, nbrow, nbcol)
    for ci in CartesianIndices(grid)
        curtree = grid[ci]
        (i, j) = Tuple(ci)
        views = zeros(Int, 4)
        # Direction 1
        for ii in i-1:-1:1
            views[1] += 1
            if grid[ii,j] >= curtree
                break
            end
        end
        # Direction 2
        for ii in i+1:nbrow
            views[2] += 1
            if grid[ii,j] >= curtree
                break
            end
        end
        # Direction 3
        for jj in j-1:-1:1
            views[3] += 1
            if grid[i,jj] >= curtree
                break
            end
        end
        # Direction 4
        for jj in j+1:nbcol
            views[4] += 1
            if grid[i,jj] >= curtree
                break
            end
        end
        scenicscores[ci] = reduce(*, views)
    end

    return maximum(scenicscores)
end


# Run part 1
display(run("./example")) # 21
display(run("./input"))

# Run part 2
display(run2("./example")) # 8
display(run2("./input"))
