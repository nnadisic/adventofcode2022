using DelimitedFiles


function run(filename::String)
    data = readlines(filename)
    score = 0
    for i in 1:length(data)
        line = split(data[i], "")
        sep = round(Int, length(line)/2) # separation index
        commonitem = first(intersect(line[1:sep], line[sep+1:end]))[1]
        score += Integer(lowercase(commonitem)) - 96
        if isuppercase(commonitem)
            score += 26
        end
    end
    return score
end

# display(run("./example"))
# display(run("./input"))

function run2(filename::String)
    data = readlines(filename)
    score = 0
    for i in 1:3:length(data)-2
        l1, l2, l3 = [data[i+k] for k in 0:2]
        commonitem = first(intersect(l1, l2, l3))[1]
        score += Integer(lowercase(commonitem)) - 96
        if isuppercase(commonitem)
            score += 26
        end
    end
    return score
end

display(run2("./example"))
display(run2("./input"))
