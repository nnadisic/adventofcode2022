using DelimitedFiles


function run(filename::String)
    data = readlines(filename)
    # Parse data to build the schedule of each pair
    schedule = zeros(Int, length(data), 4)
    for (idx, pair) in enumerate(data)
        schedule[idx, :] .= [parse(Int, a) for a in split(pair, (',', '-'))]
    end
    # Number of pairs where one assignment fully contain the other
    score = 0
    # Loop on all pairs to find such assignments
    for pair in eachrow(schedule)
        if (pair[1] <= pair[3] && pair[2] >= pair[4]) || (pair[1] >= pair[3] && pair[2] <= pair[4])
            score +=1
        end
    end
    return score
end

display(run("./example"))
display(run("./input"))


function run2(filename::String)
    data = readlines(filename)
    # Parse data to build the schedule of each pair
    schedule = zeros(Int, length(data), 4)
    for (idx, pair) in enumerate(data)
        schedule[idx, :] .= [parse(Int, a) for a in split(pair, (',', '-'))]
    end
    # Number of pairs where one assignment fully contain the other
    score = 0
    # Loop on all pairs to find such assignments
    for pair in eachrow(schedule)
        range1 = collect(pair[1]:pair[2])
        range2 = collect(pair[3]:pair[4])
        if length(intersect(range1, range2)) != 0
            score +=1
        end
    end
    return score
end

display(run2("./example"))
display(run2("./input"))
