
function run(filename::String, windowsize::Int)
    # Read data as one big string
    data = read(filename, String)
    chars = split(data, "")
    for i in windowsize:length(chars)
        if length(unique(chars[i-windowsize+1:i])) == windowsize
            return i
        end
    end
end

# Run part 1
display(run("./example", 4))
display(run("./input", 4))

# Run part 2
display(run("./example", 14))
display(run("./input", 14))
