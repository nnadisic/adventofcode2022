using DelimitedFiles

shapescore = Dict("X"=>1, # rock
                  "Y"=>2, # paper
                  "Z"=>3) # scissors

outcomescore = Dict(-1=>0, # lose
                    0=>3, # draw
                    1=>6) # win

whoequalswhom =  Dict("A"=>"X", # rock
                      "B"=>"Y", # paper
                      "C"=>"Z") # scissors

whodefeatswhom = Dict("A"=>"Z", # rock defeats scissors
                      "B"=>"X", # paper defeats rock
                      "C"=>"Y") # scissors defeats paper

wholoseswhom = Dict("A"=>"Y", # rock loses against paper
                    "B"=>"Z", # paper loses against scissors
                    "C"=>"X") # scissors loses against rock

function run(filename::String,
             wew::Dict{String, String},
             wdw::Dict{String, String},
             wlw::Dict{String, String})
    data = readdlm(filename)
    score = 0
    for round in eachrow(data)
        # Determine the outcome
        if round[2] == "X" # lose
            outcome = -1
        elseif round[2] == "Y" # lose
            outcome = 0
        elseif round[2] == "Z" # win
            outcome = 1
        end
        # Determine your shape
        if outcome == -1
            shape = wdw[round[1]]
        elseif outcome == 0
            shape = wew[round[1]]
        elseif outcome == 1
            shape = wlw[round[1]]
        end
        # Add score of the play
        score += outcomescore[outcome]
        # Add score of your shape
        score += shapescore[shape]
    end
    return score
end

display(run("./example", whoequalswhom, whodefeatswhom, wholoseswhom))
display(run("./input", whoequalswhom, whodefeatswhom, wholoseswhom))
