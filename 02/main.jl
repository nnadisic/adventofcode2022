using DelimitedFiles

shapescore = Dict("X"=>1, # rock
                  "Y"=>2, # paper
                  "Z"=>3) # scissors

outcomescore = Dict(-1=>0, # lose
                    0=>3, # draw
                    1=>6) # win

whoequalswhom =  Dict("A"=>"X", # rock
                      "B"=>"Y", # paper
                      "C"=>"Z") # scissors

whodefeatswhom = Dict("A"=>"Z", # rock defeats scissors
                      "B"=>"X", # paper defeats rock
                      "C"=>"Y") # scissors defeats paper


function play(move1::String, move2::String, wew::Dict{String, String}, wdw::Dict{String, String})
    if wew[move1] == move2
        return 0
    elseif wdw[move1] == move2
        return -1 # move1 defeats move2, so I lose
    else
        return 1 # I win
    end
end

function run(filename::String, wew::Dict{String, String}, wdw::Dict{String, String})
    data = readdlm(filename)
    score = 0
    for round in eachrow(data)
        # Add score of your shape
        score += shapescore[round[2]]
        # Add score of the play
        outcome = play(string(round[1]), string(round[2]), wew, wdw)
        score += outcomescore[outcome]
    end
    return score
end

display(run("./example", whoequalswhom, whodefeatswhom))
display(run("./input", whoequalswhom, whodefeatswhom))
